public class Main {
    public static void main(String[] args) {
        TreeClass<Integer> treeClass = new TreeClass<>(new Node(10));
        int[] array = {12,9,5,4,16,14,13,18,19,8,3,2,17};
        for (int i : array) {
            treeClass.insertNode(new Node(i));
        }
        treeClass.iterativeBFS();


    }
}
