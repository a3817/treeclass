public class Node<T extends Comparable<T>> implements Comparable<T>{
    T data;
    Node leftChild;
    Node rightChild;

    public Node(T data){
        this.data = data;
    }
    @Override
    public int compareTo(T o) {
        return data.compareTo(o);
    }

    public boolean isLargerThan(Node<T> otherNode){
        if(this.compareTo(otherNode.data) >0){
            return true;
        }else{
            return false;
        }
    }

    public boolean isEqualTo(Node<T> otherNode){
        if(data.compareTo(otherNode.data) == 0){
            return true;
        }else{
            return false;
        }
    }
}
