import org.w3c.dom.UserDataHandler;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;

public class TreeClass<T extends Comparable<T>>{
    Node<T> root;

    public TreeClass(Node root){
        this.root = root;
    }

    public void insertNode(Node insertNode){
        if(root == null){
            root = insertNode;
        }else{
            insertNodeRec(insertNode, root);
        }
    }

    private void insertNodeRec(Node insertNode, Node root){
        if (insertNode.isLargerThan(root)){
            if(root.rightChild == null){
                root.rightChild = insertNode;
            }else{
                insertNodeRec(insertNode, root.rightChild);
            }
        }else{
            if(root.leftChild == null){
                root.leftChild = insertNode;
            }else{
                insertNodeRec(insertNode, root.leftChild);
            }
        }
    }

    private void inOrderTraversalRecurive(Node root){
        if(root == null){
            //System.out.println("Empty tree");
            return;
        }
        inOrderTraversalRecurive(root.leftChild);
        System.out.println(root.data);
        inOrderTraversalRecurive(root.rightChild);
    }

    private void postOrderTraversalRecursive(Node root){
        if(root == null){
            return;
        }
        System.out.println(root.data);
        postOrderTraversalRecursive(root.leftChild);
        postOrderTraversalRecursive(root.rightChild);
    }

    public void inOrderTraversal(){
        inOrderTraversalRecurive(root);
    }

    public void postOrderTraversal(){
        postOrderTraversalRecursive(root);
    }

    private boolean searchRecursive(Node target, Node root){
        if(root == null){
            System.out.println("Target doesn't exist in the tree");
            return false;
        }else{
            if(target.isEqualTo(root)){
                System.out.println("In the tree");
                return true;
            }else if(target.isLargerThan(root)){
                //search right
                System.out.println("larger than: "+ root.data);
                return searchRecursive(target, root.rightChild);
            }else{
                System.out.println("smaller than: " + root.data);
                return searchRecursive(target, root.leftChild);
            }

        }
    }

    public boolean search(Node target){
        return searchRecursive(target, root);
    }

    public void iterativeDFS(){
        Stack<Node> searchStack = new Stack<>();
        Node traversalNode;
        searchStack.push(root);

        while(!searchStack.isEmpty()){
            traversalNode = searchStack.pop();
            System.out.println(traversalNode.data);
            if(traversalNode.rightChild != null){
                searchStack.push(traversalNode.rightChild);
            }
            if(traversalNode.leftChild != null){
                searchStack.push(traversalNode.leftChild);
            }
        }
    }

    public void iterativeBFS(){
        Queue<Node> searchQueue = new LinkedList<>();
        Node traversalNode;
        searchQueue.add(root);

        while (!searchQueue.isEmpty()) {
            traversalNode = searchQueue.poll();
            System.out.println(traversalNode.data);

            if(traversalNode.leftChild != null){
                searchQueue.add(traversalNode.leftChild);

            }
            if(traversalNode.rightChild != null)
                searchQueue.add(traversalNode.rightChild);
            }
        }


}
